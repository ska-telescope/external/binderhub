-include .make/base.mk
# include makefile targets for Oci images management
-include .make/oci.mk
# include makefile targets for versioning management
-include .make/release.mk